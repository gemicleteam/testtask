$(function() {
    $('#form').on('submit', onSearch);
});

var map;
var path;

var marker;
var markers = [];

function onSearch(e) {
  e.preventDefault();

  var coords = markers.map(function(marker) {
    return new google.maps.LatLng(marker.position.lat(), marker.position.lng())
  });

  var bounds = new google.maps.LatLngBounds();
  coords.forEach(function(coord) {
    bounds.extend(coord);
  });

  var service = new google.maps.places.PlacesService(window.map);
  service.nearbySearch({
    bounds: bounds,
    keyword: e.target.elements.search.value
  }, function(list) {
    var $container = $('.results');
    $container.html('');

    list
      .filter(function(item) {
        return isInPolygon(item.geometry.location);
      })
      .slice(0, 10)
      .map(function(item){
        var $element = $(`<div>${item.name} (${item.vicinity})</div>`);
        $container.append($element);
      });
    if (!$container.children().length) {
      $container.html('Not found');
    }
  });
}

function initMap() {
  var myLatlng = new google.maps.LatLng(25.874, -80.200);
  var myOptions = {
    zoom: 12,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

  google.maps.event.addListener(window.map, 'click', function(point) {
    addPointToPolygon(point['latLng']);
  });

  updatePoly();
}

function updatePoly() {
  var coords = markers.map(function(marker) {
    return new google.maps.LatLng(marker.position.lat(), marker.position.lng())
  });

  path = new google.maps.Polygon({
    path: coords,
    geodesic: true,
    strokeColor: '#17C4BB',
    strokeOpacity: 1.0,
    strokeWeight: 2,
    fillColor: '#17C4BB',
    fillOpacity: 0.4
  });

  path.setMap(map);
}

function addPointToPolygon(latLng) {
  marker = new google.maps.Marker({
    position: latLng,
    map: window.map,
    draggable: true
  });

  markers.push(marker);

  google.maps.event.addListener(marker, 'click', removeMarkerFromPolygon.bind(marker));

  google.maps.event.addListener(marker, 'dragend', function () {
    updatePoly();
  });

  path.setMap(null);
  updatePoly();
}

function removeMarkerFromPolygon() {
  this.setMap(null);

  for (var i = 0; i < markers.length; i++) {
    if (markers[i] == this) {
      console.log('markers[i]', markers[i]);
      markers.splice(i, 1);
    }
  }
  path.setMap(null);
  updatePoly();
}

function isInPolygon(point) {
  return google.maps.geometry.poly.containsLocation(point, path);
}